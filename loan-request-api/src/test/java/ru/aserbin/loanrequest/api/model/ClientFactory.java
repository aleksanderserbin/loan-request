package ru.aserbin.loanrequest.api.model;

import ru.aserbin.loanrequest.model.ClientData;

public interface ClientFactory extends Factory<ClientData> {
}
