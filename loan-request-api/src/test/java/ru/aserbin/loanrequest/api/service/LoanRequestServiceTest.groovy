package ru.aserbin.loanrequest.api.service

import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration
import ru.aserbin.loanrequest.api.exception.NotFoundException
import ru.aserbin.loanrequest.api.model.ClientFactory
import ru.aserbin.loanrequest.api.model.LoanRequestConfigFactory
import ru.aserbin.loanrequest.api.model.LoanrequestFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultClientFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultLoanRequestConfigurationFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultLoanRequestFactory
import ru.aserbin.loanrequest.api.repository.LoanRequestRepository
import ru.aserbin.loanrequest.model.LoanRequest
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class LoanRequestServiceTest extends Specification {

    private LoanrequestFactory loanrequestFactory;
    private ClientFactory clientFactory;
    private LoanRequestConfigFactory loanRequestConfigFactory;


    private static final LocalDateTime DEFAULT_TEST_DATE = LocalDate.now().atTime(10, 0);
    private static final LocalDateTime EARLY_TIME_TRANSCATION = LocalDate.now().atTime(2, 0);
    private static final Long STUB_LOAN_REQUEST_ID = 1e6;


    void setup() {
        this.clientFactory = new DefaultClientFactory();
        this.loanrequestFactory = new DefaultLoanRequestFactory(this.clientFactory);
        this.loanRequestConfigFactory = new DefaultLoanRequestConfigurationFactory();
    }

    void cleanup() {

    }

    def "Loan request should be approved if requirements are met"() {
        given: "the correct loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance()

        and: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.findByClientAndDate(_, _) >> Collections.emptyList()
        loanRequestRepository.save(_) >> { loanRequest }

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequest = loanRequestService.makeRequest(loanRequest)

        then: "we expect it to be approved"
        loanRequest.getStatus() == LoanRequest.LoanRequestStatus.APPROVED
    }

    def "Loan request should be rejected if request is made with max amount and between 0:00 and 6:00 AM"() {
        given: "the correct loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance()
        loanRequest.setRequestTime(EARLY_TIME_TRANSCATION)

        and: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.findByClientAndDate(_, _) >> Collections.emptyList()
        loanRequestRepository.save(_) >> { loanRequest }

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequest = loanRequestService.makeRequest(loanRequest)

        then: "we expect it to be rejected"
        loanRequest.getStatus() == LoanRequest.LoanRequestStatus.REJECTED
    }

    def "Loan request should be rejected if request is made more than max times for one client and IPAddr"() {
        given: "the correct loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance()

        and: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.findByClientAndDate(_, _) >> Arrays.asList(loanRequest, loanRequest)
        loanRequestRepository.save(_) >> { loanRequest }

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();
        configuration.setMaxTriesPerDay(1)

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequest = loanRequestService.makeRequest(loanRequest)

        then: "we expect it to be rejected"
        loanRequest.getStatus() == LoanRequest.LoanRequestStatus.REJECTED
    }

    def "Loan request should be approved if requirements are met including override with local time"() {
        given: "the correct loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance()
        loanRequest.setRequestTime(EARLY_TIME_TRANSCATION)
        loanRequest.setLocalTime(DEFAULT_TEST_DATE)

        and: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.findByClientAndDate(_, _) >> Arrays.asList(loanRequest)
        loanRequestRepository.save(_) >> { loanRequest }

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequest = loanRequestService.makeRequest(loanRequest)

        then: "we expect it to be approved"
        loanRequest.getStatus() == LoanRequest.LoanRequestStatus.APPROVED
    }

    def "FindById should return the target entity if it is found "() {
        given: "the correct loan request"
        LoanRequest loanRequest = loanrequestFactory.getInstance()

        and: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.find(_) >> loanRequest

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequest = loanRequestService.findById(STUB_LOAN_REQUEST_ID)

        then: "we expect NotFoundException to be thrown"
        loanRequest != null
    }

    def "FindById should throw NotFoundException in case there is no such object"() {
        given: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.find(_) >> null

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        loanRequestService.findById(1)

        then: "we expect NotFoundException to be thrown"
        thrown NotFoundException
    }

    def "FindAll should return empty list if there are no entities in the DB"() {
        given: "Loan request repository"
        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Collections.emptyList()
        loanRequestRepository.find(_) >> null

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        List<LoanRequest> loanRequests = loanRequestService.findAll()

        then: "we expect size of the response to be 0"
        loanRequests.size() == 0
    }

    def "FindAll should return all entities the DB"() {
        given: "the correct loan request"
        LoanRequest loanRequest = loanrequestFactory.getInstance()

        and: "Loan request repository"

        LoanRequestRepository loanRequestRepository = Mock(LoanRequestRepository.class);
        loanRequestRepository.findAll() >> Arrays.asList(loanRequest, loanRequest, loanRequest)

        and: "Request loan service configuration"
        LoanRequestConfiguration configuration = loanRequestConfigFactory.getInstance();

        and: "LoanRequestService which is under test"
        LoanRequestService loanRequestService = new LoanRequestServiceImpl(loanRequestRepository, configuration)

        when: "we are trying to make the loan request"
        List<LoanRequest> loanRequests = loanRequestService.findAll()

        then: "we expect size of the response to be amount of all entities"
        loanRequests.size() == 3
    }
}
