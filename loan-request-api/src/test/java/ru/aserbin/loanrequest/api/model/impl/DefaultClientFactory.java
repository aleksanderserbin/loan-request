package ru.aserbin.loanrequest.api.model.impl;

import ru.aserbin.loanrequest.api.model.ClientFactory;
import ru.aserbin.loanrequest.model.ClientData;

import java.util.UUID;

public class DefaultClientFactory implements ClientFactory {

    private static final String DEFAULT_NAME = "John";
    private static final String DEFAULT_LAST_NAME = "Doe";
    private static final Integer DEFAULT_SALARY = 1000;
    private static final String DEFAULT_ID = UUID.randomUUID().toString();

    public ClientData getInstance() {
        ClientData clientData = new ClientData();
        clientData.setDocumentId(DEFAULT_ID);
        clientData.setName(DEFAULT_NAME);
        clientData.setLastName(DEFAULT_LAST_NAME);
        clientData.setSalary(DEFAULT_SALARY);
        return clientData;
    }

}
