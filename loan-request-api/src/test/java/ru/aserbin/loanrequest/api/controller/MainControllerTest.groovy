package ru.aserbin.loanrequest.api.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration
import ru.aserbin.loanrequest.api.model.ClientFactory
import ru.aserbin.loanrequest.api.model.LoanrequestFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultClientFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultLoanRequestConfigurationFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultLoanRequestFactory
import ru.aserbin.loanrequest.api.service.LoanRequestService
import ru.aserbin.loanrequest.api.service.LoanRequestServiceImpl
import ru.aserbin.loanrequest.api.validation.LoanRequestValidator
import ru.aserbin.loanrequest.model.LoanRequest
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest
import java.time.LocalDateTime

class MainControllerTest extends Specification {

    private LoanrequestFactory loanRequestFactory;
    private ClientFactory clientFactory;
    private LoanRequestValidator loanRequestValidator;
    private LoanRequestConfiguration loanRequestConfiguration;

    private static final String ALTERNATE_IP_ADDR = "10.0.0.0";
    private static final String FAKE_IP_ADDR = "11.0.0.0";
    private static final FAKE_REQUEST_DATE = LocalDateTime.now().minusDays(1);

    void setup() {
        this.loanRequestConfiguration = new DefaultLoanRequestConfigurationFactory().getInstance();
        this.clientFactory = new DefaultClientFactory();
        this.loanRequestFactory = new DefaultLoanRequestFactory(clientFactory);
        this.loanRequestValidator = new LoanRequestValidator(loanRequestConfiguration);
    }

    def "RequestLoan should set IP address of the client connection"() {
        given: "Loan request with empty IPAddr"
        LoanRequest loanRequest = loanRequestFactory.getInstance();
        loanRequest.setIPAddr(null)

        and: "Http request"
        HttpServletRequest httpServletRequest = Mock(HttpServletRequest.class)
        httpServletRequest.getRemoteAddr() >> ALTERNATE_IP_ADDR

        and: "Loan request Service"
        LoanRequestService loanRequestService = Mock(LoanRequestServiceImpl.class)
        loanRequestService.makeRequest(_) >> { it -> it.get(0) }

        and: "MainController which is under test"
        MainController mainController = new MainController(loanRequestValidator, loanRequestService);

        when: "We send request to the controller"
        ResponseEntity response = mainController.requestLoan(loanRequest, httpServletRequest)

        then: "We expect IPAddr to be set and HTTP code to be 200"
        response.statusCode == HttpStatus.OK
        ALTERNATE_IP_ADDR == ((LoanRequest) response.getBody()).getIPAddr()

    }

}
