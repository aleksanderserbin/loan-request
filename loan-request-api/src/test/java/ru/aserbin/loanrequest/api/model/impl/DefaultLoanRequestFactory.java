package ru.aserbin.loanrequest.api.model.impl;

import ch.qos.logback.core.net.server.Client;
import ru.aserbin.loanrequest.api.model.ClientFactory;
import ru.aserbin.loanrequest.api.model.LoanrequestFactory;
import ru.aserbin.loanrequest.model.LoanRequest;

public class DefaultLoanRequestFactory implements LoanrequestFactory {

    private static final Integer DEFAULT_LOAN_AMOUNT = 100;
    private static final Byte DEFAULT_LOAN_TERM = 10;
    private static final String DEFAULT_LOAN_IP = "127.0.0.1";

    private ClientFactory clientFactory;

    public DefaultLoanRequestFactory(ClientFactory clientFactory) {
        this.clientFactory = clientFactory;
    }

    public LoanRequest getInstance() {
        LoanRequest loanRequest = new LoanRequest();
        loanRequest.setAmount(DEFAULT_LOAN_AMOUNT);
        loanRequest.setIPAddr(DEFAULT_LOAN_IP);
        loanRequest.setTerm (DEFAULT_LOAN_TERM);
        loanRequest.setClient(clientFactory.getInstance());
        return loanRequest;
    }
}
