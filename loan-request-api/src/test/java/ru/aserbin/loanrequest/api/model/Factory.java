package ru.aserbin.loanrequest.api.model;

public interface Factory<T> {
    T getInstance();
}
