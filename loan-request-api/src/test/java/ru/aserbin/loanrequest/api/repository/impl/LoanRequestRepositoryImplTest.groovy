package ru.aserbin.loanrequest.api.repository.impl

import org.hibernate.PropertyValueException
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.cfg.Configuration
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder
import ru.aserbin.loanrequest.api.model.ClientFactory
import ru.aserbin.loanrequest.api.model.LoanrequestFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultClientFactory
import ru.aserbin.loanrequest.api.model.impl.DefaultLoanRequestFactory
import ru.aserbin.loanrequest.api.repository.LoanRequestRepository
import ru.aserbin.loanrequest.model.ClientData
import ru.aserbin.loanrequest.model.LoanRequest
import spock.lang.Specification
import sun.reflect.generics.reflectiveObjects.NotImplementedException

import java.time.LocalDate
import java.time.LocalDateTime

class LoanRequestRepositoryImplTest extends Specification {

    private SessionFactory sessionFactory;
    private LoanrequestFactory loanrequestFactory;
    private ClientFactory clientFactory;

    private static final LocalDateTime DEFAULT_TEST_DATE = LocalDate.now().atTime(10, 0);
    private static final LocalDateTime INCORRECT_FIND_BY_DATE_VALUE = DEFAULT_TEST_DATE.minusDays(10);
    private static final LocalDateTime CORRECT_FIND_BY_DATE_VALUE = DEFAULT_TEST_DATE.plusHours(10);
    private static final String ALTERNATIVE_CLIENT_ID = "_"

    void setup() {

        this.clientFactory = new DefaultClientFactory();
        this.loanrequestFactory = new DefaultLoanRequestFactory(clientFactory);

        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase dataSource = builder
                .setType(EmbeddedDatabaseType.HSQL)
                .build();

        Configuration hibernateConfiguration = new LocalSessionFactoryBuilder(dataSource);
        hibernateConfiguration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect")
        hibernateConfiguration.setProperty("hibernate.hbm2ddl.auto", "create-drop")
        hibernateConfiguration.setProperty("javax.persistence.validation.mode", "ddl")
        hibernateConfiguration.setProperty("hibernate.show_sql", "true")
        hibernateConfiguration.setProperty("hibernate.default_schema", "PUBLIC")
        hibernateConfiguration.addAnnotatedClasses(LoanRequest.class,
                ClientData.class)
        sessionFactory = hibernateConfiguration.buildSessionFactory();

    }

    def "Correctly filled LoanRequest should be saved to the database"() {
        given: "the correctly filled loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance();

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "we are trying to save the loan request"
        LoanRequest savedLoanRequest = loanRequestRepository.save(loanRequest)

        then: "we expect it to be saved with newly generated ID"
        savedLoanRequest.getId() != null

    }

    def "Incorrectly filled LoanRequest should throw PropertyValueException on save"() {
        given: "the correctly filled loan request"

        LoanRequest loanRequest = loanrequestFactory.getInstance()
        loanRequest.setTerm(null)

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "we are trying to save the loan request"
        loanRequestRepository.save(loanRequest)

        then: "we expect it to throw PropertyValueException"

        thrown PropertyValueException

    }

    def "Find LoanRequest by id should return single entity if it is present in the database"() {
        given: "Predefined entity in the db"
        LoanRequest loanRequest = loanrequestFactory.getInstance()
        Session session = sessionFactory.openSession();
        Long requestId = (Long) session.save(loanRequest);
        session.flush()

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        LoanRequest retrievedLoanRequest = loanRequestRepository.find(requestId);

        then:
        retrievedLoanRequest != null
        retrievedLoanRequest.getId() == requestId
    }


    def "Find LoanRequest by id should return null if entity is not present in the database"() {
        given: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        LoanRequest retrievedLoanRequest = loanRequestRepository.find(1);

        then:
        retrievedLoanRequest == null
    }

    def "FindAll should return all entities which are present in the database"() {
        given: "Predefined entities in the db"
        LoanRequest loanRequest1 = loanrequestFactory.getInstance()
        LoanRequest loanRequest2 = loanrequestFactory.getInstance()
        loanRequest2.getClient().setDocumentId(ALTERNATIVE_CLIENT_ID)
        Session session = sessionFactory.openSession();
        session.save(loanRequest1);
        session.save(loanRequest2)
        session.flush()

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        List<LoanRequest> loanRequests = loanRequestRepository.findAll();

        then:
        loanRequests != null
        loanRequests.size() > 0
    }


    def "FindAll should return empty list if there are no entities in the database"() {
        given: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        List<LoanRequest> loanRequests = loanRequestRepository.findAll();

        then:
        loanRequests != null
        loanRequests.size() == 0
    }

    def "FindByClient should return all entities which match by client from the database"() {
        given: "Predefined entities in the db"
        LoanRequest loanRequest1 = loanrequestFactory.getInstance()
        LoanRequest loanRequest2 = loanrequestFactory.getInstance()
        LoanRequest loanRequest3 = loanrequestFactory.getInstance()
        loanRequest3.getClient().setDocumentId(ALTERNATIVE_CLIENT_ID)
        ClientData client = loanRequest1.getClient();
        Session session = sessionFactory.openSession();
        session.save(loanRequest1);
        session.merge(loanRequest2);
        session.save(loanRequest3);
        session.flush()

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        List<LoanRequest> loanRequests = loanRequestRepository.findByClient(client);

        then:
        loanRequests != null
        loanRequests.size() == 2
    }

    def "FindByClient should return empty list if there are no entities in the database"() {
        given: "LoanRequest entity"
        LoanRequest loanRequest = loanrequestFactory.getInstance();
        Session session = sessionFactory.openSession();
        session.save(loanRequest);

        and: "Client which is not persisted in the database"
        ClientData client = clientFactory.getInstance();
        client.setDocumentId(ALTERNATIVE_CLIENT_ID)

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entity"
        List<LoanRequest> loanRequests = loanRequestRepository.findByClient(client);

        then:
        loanRequests != null
        loanRequests.size() == 0
    }

    def "FindByClientAndDate should return all entities from the database which match by date (not time) and client"() {
        given: "Predefined entities in the db"
        LoanRequest anotherClientLoanRequest = loanrequestFactory.getInstance()
        LoanRequest incorrectTimeLoanRequest = loanrequestFactory.getInstance()
        LoanRequest correctLoanRequestWithModifiedDate = loanrequestFactory.getInstance()
        LoanRequest defaultLoanRequest = loanrequestFactory.getInstance()
        anotherClientLoanRequest.getClient().setDocumentId(ALTERNATIVE_CLIENT_ID);
        incorrectTimeLoanRequest.setRequestTime(INCORRECT_FIND_BY_DATE_VALUE);
        correctLoanRequestWithModifiedDate.setRequestTime(CORRECT_FIND_BY_DATE_VALUE);
        ClientData client = correctLoanRequestWithModifiedDate.getClient();
        Session session = sessionFactory.openSession();
        session.merge(anotherClientLoanRequest);
        session.merge(incorrectTimeLoanRequest);
        correctLoanRequestWithModifiedDate = (LoanRequest) session.merge(correctLoanRequestWithModifiedDate);
        defaultLoanRequest = (LoanRequest) session.merge(defaultLoanRequest);
        session.flush()

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entities"
        List<LoanRequest> loanRequests = loanRequestRepository.findByClientAndDate(client, DEFAULT_TEST_DATE);

        then: "We expect to see matching entities"
        loanRequests != null
        loanRequests.size() == 2
        loanRequests.findAll( {it.getId() == correctLoanRequestWithModifiedDate.getId()} ).size() > 0
        loanRequests.findAll( {it.getId() == defaultLoanRequest.getId()} ).size() > 0
    }

    def "FindByClientAndDate should return empty list if there are no entities match"() {
        given: "Predefined entities in the db"
        LoanRequest loanRequest = loanrequestFactory.getInstance()
        loanRequest.setRequestTime(INCORRECT_FIND_BY_DATE_VALUE);
        ClientData client = loanRequest.getClient();
        Session session = sessionFactory.openSession();
        session.save(loanRequest);
        session.flush()

        and: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        when: "We are trying to get the entities"
        List<LoanRequest> loanRequests = loanRequestRepository.findByClientAndDate(client, LocalDateTime.now());

        then: "We expect to see matching entities"
        loanRequests != null
        loanRequests.size() == 0
    }

    def "FindByClientAndDate should return empty list if there are no entities in the DB"() {
        given: "LoanRequestRepository which is under test"
        LoanRequestRepository loanRequestRepository = new LoanRequestRepositoryImpl(sessionFactory)

        and: "Client, which i snot persisted in the database"
        ClientData client = clientFactory.getInstance()

        when: "We are trying to get the entities"
        List<LoanRequest> loanRequests = loanRequestRepository.findByClientAndDate(client, LocalDateTime.now());

        then: "We expect to see matching entities"
        loanRequests != null
        loanRequests.size() == 0
    }
}
