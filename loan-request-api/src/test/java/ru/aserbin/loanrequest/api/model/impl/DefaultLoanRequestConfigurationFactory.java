package ru.aserbin.loanrequest.api.model.impl;

import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration;
import ru.aserbin.loanrequest.api.model.LoanRequestConfigFactory;

public class DefaultLoanRequestConfigurationFactory implements LoanRequestConfigFactory{

    private static final Double DEFAULT_MAX_LOAN_REQUEST_AMOUNT = 100.0;
    private static final Integer DEFAULT_MAX_TRIES_PER_DAY = 3;
    private static final Double DEFAULT_RISK_THRESHOLD = 0.8;
    private static final Boolean DEFAULT_ALLOW_LCAL_TIME_OVERRIDE = true;


    @Override
    public LoanRequestConfiguration getInstance() {
        LoanRequestConfiguration configuration = new LoanRequestConfiguration();
        configuration.setMaxLoanRequestAmount(DEFAULT_MAX_LOAN_REQUEST_AMOUNT);
        configuration.setMaxTriesPerDay(DEFAULT_MAX_TRIES_PER_DAY);
        configuration.setRiskThreshold(DEFAULT_RISK_THRESHOLD);
        configuration.setAllowLocalTimeOverride(DEFAULT_ALLOW_LCAL_TIME_OVERRIDE);
        return configuration;
    }
}
