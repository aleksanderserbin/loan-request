package ru.aserbin.loanrequest.api.model;

import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration;

public interface LoanRequestConfigFactory extends Factory<LoanRequestConfiguration> {
}
