package ru.aserbin.loanrequest.api.configuration;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import javax.sql.DataSource;

@Profile("liquibase")
@Configuration
public class LiquibaseConfig extends ParentDataSourceConfig{

    @Value("${liquibase.user}")
    private String liquibaseUser;


    @Value("${liquibase.password}")
    private String liquibasePassword;

    @Bean
    public DataSource liquibaseDatasource() throws ClassNotFoundException {
        SingleConnectionDataSource dataSource = new SingleConnectionDataSource();
        dataSource.setUsername(liquibaseUser);
        dataSource.setPassword(liquibasePassword);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setDriverClassName(driverClassName);
        return dataSource;
    }

    @Bean
    public SpringLiquibase liquibase() throws ClassNotFoundException {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:liquibase/master.xml");
        liquibase.setDataSource(liquibaseDatasource());
        return liquibase;
    }
}
