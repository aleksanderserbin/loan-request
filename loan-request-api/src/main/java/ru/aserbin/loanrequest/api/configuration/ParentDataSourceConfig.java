package ru.aserbin.loanrequest.api.configuration;

import org.springframework.beans.factory.annotation.Value;

public class ParentDataSourceConfig {

    @Value("${jdbc.password}")
    protected String dataSourcePassword;

    @Value("${jdbc.user}")
    protected String dataSourceUsername;

    @Value("${jdbc.url}")
    protected String dataSourceUrl;

    @Value("${jdbc.driver}")
    protected String driverClassName;
}
