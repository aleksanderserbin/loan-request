package ru.aserbin.loanrequest.api.service;

import ru.aserbin.loanrequest.model.LoanRequest;

import java.util.List;

public interface LoanRequestService {

    LoanRequest makeRequest(LoanRequest loanRequest);
    List<LoanRequest> findAll();
    LoanRequest findById(Long id);

}
