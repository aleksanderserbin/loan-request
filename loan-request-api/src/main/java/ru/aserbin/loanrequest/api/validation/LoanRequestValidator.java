package ru.aserbin.loanrequest.api.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration;
import ru.aserbin.loanrequest.model.ClientData;
import ru.aserbin.loanrequest.model.LoanRequest;

import javax.validation.ConstraintViolation;
import javax.validation.executable.ExecutableValidator;
import javax.validation.metadata.BeanDescriptor;
import java.time.Duration;
import java.time.Period;
import java.util.Set;

@Component
public class LoanRequestValidator implements Validator {

    private LoanRequestConfiguration loanRequestConfiguration;

    @Autowired
    public LoanRequestValidator(LoanRequestConfiguration loanRequestConfiguration) {
        this.loanRequestConfiguration = loanRequestConfiguration;
    }

    public boolean supports(Class<?> clazz) {
        return LoanRequest.class.equals(clazz);
    }

    public void validate(Object object, Errors errors) {
        LoanRequest loanRequest = (LoanRequest) object;

        if (loanRequest.getAmount() == null) {
            errors.reject("Amount", "Amount is missing");
        } else {
            if (loanRequest.getAmount() < 0 || loanRequest.getAmount() < loanRequestConfiguration.getMinimalLoanRequestAmount()
                    || loanRequest.getAmount() > loanRequestConfiguration.getMaxLoanRequestAmount()) {
                errors.reject("Amount", "Specified amount is out of appropriate range");
            }
        }

        if (loanRequest.getTerm() == null) {
            errors.reject("Term", "Term is missing");
        } else if (loanRequest.getTerm() < 0
                    || loanRequest.getTerm() < loanRequestConfiguration.getMinLoanRequestDurationMonths()
                    || loanRequest.getTerm() > loanRequestConfiguration.getMaxLoanRequestDurationMonths()) {
                errors.reject("Term", "Specified term is out of appropriate range");
        }

        if (loanRequest.getClient() == null) {
            errors.reject("Client information", "Client information is missing");
        } else {
            ClientData client = loanRequest.getClient();

            if (client.getSalary() == null) {
                errors.reject("Salary", "Salary information is missing");
            }

            if (client.getName() == null) {
                errors.reject("Name", "Client name information is missing");
            }

            if (client.getLastName() == null) {
                errors.reject("Last Name", "Client last name information is missing");
            }
        }

    }
}
