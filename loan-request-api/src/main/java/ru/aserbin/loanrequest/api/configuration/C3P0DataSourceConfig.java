package ru.aserbin.loanrequest.api.configuration;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

@Configuration
public class C3P0DataSourceConfig extends ParentDataSourceConfig{

    @Value("${jdbc.maxPoolSize}")
    private Integer maxPoolSize;

    @Value("${jdbc.initPoolSize}")
    private Integer initPoolSize;

    @Value("${jdbc.maxIdleTime}")
    private Integer maxIdleTime;

    @Bean
    @Primary
    public DataSource dataSource() throws PropertyVetoException {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setUser(dataSourceUsername);
        dataSource.setPassword(dataSourcePassword);
        dataSource.setDriverClass(driverClassName);
        dataSource.setJdbcUrl(dataSourceUrl);
        dataSource.setMaxPoolSize(maxPoolSize);
        dataSource.setInitialPoolSize(initPoolSize);
        dataSource.setMaxIdleTime(maxIdleTime);
        dataSource.setDebugUnreturnedConnectionStackTraces(true);
        dataSource.setUnreturnedConnectionTimeout(3000);
        return dataSource;
    }
}
