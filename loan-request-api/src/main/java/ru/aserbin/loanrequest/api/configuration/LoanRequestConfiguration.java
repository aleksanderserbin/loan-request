package ru.aserbin.loanrequest.api.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoanRequestConfiguration {

    @Value("${api.allowLocalTimeOverride}")
    private Boolean allowLocalTimeOverride;

    @Value("${loan.maxTriesPerDay}")
    private Integer maxTriesPerDay;

    @Value("${loan.risk.threshold}")
    private Double riskThreshold;

    @Value("${loan.request.amount.min}")
    private Double minimalLoanRequestAmount;

    @Value("${loan.request.amount.max}")
    private Double maxLoanRequestAmount;

    @Value("${loan.request.duration.min}")
    private Double minLoanRequestDurationMonths;

    @Value("${loan.request.duration.max}")
    private Double maxLoanRequestDurationMonths;

    public Boolean getAllowLocalTimeOverride() {
        return allowLocalTimeOverride;
    }

    public void setAllowLocalTimeOverride(Boolean allowLocalTimeOverride) {
        this.allowLocalTimeOverride = allowLocalTimeOverride;
    }

    public Integer getMaxTriesPerDay() {
        return maxTriesPerDay;
    }

    public void setMaxTriesPerDay(Integer maxTriesPerDay) {
        this.maxTriesPerDay = maxTriesPerDay;
    }

    public Double getRiskThreshold() {
        return riskThreshold;
    }

    public void setRiskThreshold(Double riskThreshold) {
        this.riskThreshold = riskThreshold;
    }

    public Double getMinimalLoanRequestAmount() {
        return minimalLoanRequestAmount;
    }

    public void setMinimalLoanRequestAmount(Double minimalLoanRequestAmount) {
        this.minimalLoanRequestAmount = minimalLoanRequestAmount;
    }

    public Double getMaxLoanRequestAmount() {
        return maxLoanRequestAmount;
    }

    public void setMaxLoanRequestAmount(Double maxLoanRequestAmount) {
        this.maxLoanRequestAmount = maxLoanRequestAmount;
    }

    public Double getMinLoanRequestDurationMonths() {
        return minLoanRequestDurationMonths;
    }

    public void setMinLoanRequestDurationMonths(Double minLoanRequestDurationMonths) {
        this.minLoanRequestDurationMonths = minLoanRequestDurationMonths;
    }

    public Double getMaxLoanRequestDurationMonths() {
        return maxLoanRequestDurationMonths;
    }

    public void setMaxLoanRequestDurationMonths(Double maxLoanRequestDurationMonths) {
        this.maxLoanRequestDurationMonths = maxLoanRequestDurationMonths;
    }
}
