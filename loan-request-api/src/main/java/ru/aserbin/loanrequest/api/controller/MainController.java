package ru.aserbin.loanrequest.api.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.aserbin.loanrequest.api.service.LoanRequestService;
import ru.aserbin.loanrequest.api.validation.LoanRequestValidator;
import ru.aserbin.loanrequest.model.LoanRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.SQLException;
import java.util.List;

@RestController
public class MainController {

    private LoanRequestService service;
    private LoanRequestValidator loanRequestValidator;

    @Autowired
    public MainController(LoanRequestValidator loanRequestValidator,
                          LoanRequestService loanRequestService) {
        this.service = loanRequestService;
        this.loanRequestValidator = loanRequestValidator;
    }

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(loanRequestValidator);
    }

    @RequestMapping(value = {"/request/", "/request"}, method = RequestMethod.POST)
    public ResponseEntity requestLoan(@RequestBody @Valid LoanRequest loanRequest,
                                      HttpServletRequest request) throws SQLException {
        loanRequest.setIPAddr(request.getRemoteAddr());
        LoanRequest loanRequestResponse = service.makeRequest(loanRequest);
        return ResponseEntity.ok(loanRequestResponse);
    }

    @RequestMapping(value = {"/request/", "/request"}, method = RequestMethod.GET)
    public ResponseEntity<List<LoanRequest>> get() {
        List<LoanRequest> loanRequests = service.findAll();
        return ResponseEntity.ok(loanRequests);
    }

    @RequestMapping(value =  {"/request/{id}/", "/request/{id}"}, method = RequestMethod.GET)
    public ResponseEntity<LoanRequest> getRequestById(@PathVariable("id") Long requestId) {
        LoanRequest loanRequest = service.findById(requestId);
        return ResponseEntity.ok(loanRequest);
    }


}
