package ru.aserbin.loanrequest.api.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.aserbin.loanrequest.api.repository.AbstractRepository;
import ru.aserbin.loanrequest.api.repository.LoanRequestRepository;
import ru.aserbin.loanrequest.model.ClientData;
import ru.aserbin.loanrequest.model.LoanRequest;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Component
public class LoanRequestRepositoryImpl extends AbstractRepository
        implements LoanRequestRepository {

    private final Logger logger
            = LoggerFactory.getLogger(LoanRequestRepositoryImpl.class);

    private static final String FIND_BY_CLIENT_AND_DATE_QUERY =
            "from LoanRequest l where l.client = :client AND DAY(l.requestTime) = DAY(:date) AND " +
            "MONTH(l.requestTime) = MONTH(:date) AND YEAR(l.requestTime) = YEAR(:date)";

    public LoanRequestRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public LoanRequest save(LoanRequest obj) {
        Session session = getSession();
        return (LoanRequest) session.merge(obj);
    }

    public LoanRequest update(LoanRequest obj) {
        throw new NotImplementedException();
    }

    public LoanRequest find(Long key) {
        Session session = getSession();
        return session.get(LoanRequest.class, key);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<LoanRequest> findAll() {
        Session session = getSession();
        List dbResponse = session.createCriteria(LoanRequest.class).list();
        List<LoanRequest> loanRequests = new ArrayList<>(dbResponse.size());
        dbResponse.forEach(req -> loanRequests.add((LoanRequest) req));
        return loanRequests;
    }

    public void delete(LoanRequest obj) {
        throw new NotImplementedException();
    }

    @Override
    public List<LoanRequest> findByClient(ClientData clientData) {
        Session session = getSession();
        List clientRequest = session.createQuery("from LoanRequest l where l.client = :client")
                .setParameter("client", clientData)
                .list();
        return clientRequest;
    }

    @Override
    public List<LoanRequest> findByClientAndDate(ClientData clientData, LocalDateTime date) {
        Session session = getSession();
        List clientRequest = session.createCriteria(LoanRequest.class)
                .add(Restrictions.gt("requestTime", date.truncatedTo(ChronoUnit.DAYS)))
                .add(Restrictions.lt("requestTime", date.plusDays(1).truncatedTo(ChronoUnit.DAYS)))
                .add(Restrictions.eq("client.documentId", clientData.getDocumentId()))
                .list();
        return clientRequest;
    }

}
