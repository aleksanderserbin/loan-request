package ru.aserbin.loanrequest.api.repository;

import ru.aserbin.loanrequest.model.ClientData;
import ru.aserbin.loanrequest.model.LoanRequest;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface LoanRequestRepository extends Repository<LoanRequest, Long> {

    List<LoanRequest> findByClient(ClientData clientData);
    List<LoanRequest> findByClientAndDate(ClientData clientData, LocalDateTime date);
}
