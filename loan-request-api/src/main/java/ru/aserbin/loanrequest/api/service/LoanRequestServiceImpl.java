package ru.aserbin.loanrequest.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.aserbin.loanrequest.api.configuration.LoanRequestConfiguration;
import ru.aserbin.loanrequest.api.exception.NotFoundException;
import ru.aserbin.loanrequest.api.repository.LoanRequestRepository;
import ru.aserbin.loanrequest.model.LoanRequest;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class LoanRequestServiceImpl implements LoanRequestService {

    private final Logger logger
            = LoggerFactory.getLogger(LoanRequestServiceImpl.class);

    private LoanRequestRepository loanRequestRepository;
    private LoanRequestConfiguration loanRequestConfiguration;

    @Autowired
    public LoanRequestServiceImpl(LoanRequestRepository loanRequestRepository,
                                  LoanRequestConfiguration loanRequestConfiguration) {
        this.loanRequestRepository = loanRequestRepository;
        this.loanRequestConfiguration = loanRequestConfiguration;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public LoanRequest makeRequest(LoanRequest loanRequest) {
        if (loanRequestConfiguration.getAllowLocalTimeOverride() && loanRequest.getLocalTime() != null) {
            loanRequest.setRequestTime(loanRequest.getLocalTime());
        }
        loanRequest.setStatus(LoanRequest.LoanRequestStatus.APPROVED);
        Double risk = getRisk(loanRequest);
        if (risk >  loanRequestConfiguration.getRiskThreshold()) {
            loanRequest.setStatus(LoanRequest.LoanRequestStatus.REJECTED);
            logger.debug(String.format("Request %s was rejected", loanRequest));
        }
        return loanRequestRepository.save(loanRequest);
    }

    public List<LoanRequest> findAll() {
        return loanRequestRepository.findAll();
    }

    public LoanRequest findById(Long id) {
        LoanRequest loanRequest = loanRequestRepository.find(id);
        if (loanRequest == null) {
            throw new NotFoundException("Not found");
        }
        return loanRequest;
    }

    private Double getRisk(LoanRequest loanRequest) {
        List<LoanRequest> thisDayRequests =
                loanRequestRepository.findByClientAndDate(loanRequest.getClient(), LocalDateTime.now());

        if (thisDayRequests.size() >= loanRequestConfiguration.getMaxTriesPerDay()) {
            logger.debug(String.format("Request %s exceeds max tries per day. Rejecting.", loanRequest));
            return 1.;
        }

        if (loanRequest.getRequestTime().getHour() < 6
                && loanRequest.getAmount() > loanRequestConfiguration.getMaxLoanRequestAmount() * 0.95) {
            logger.debug(String.format("Request %s is done at suspicious time with big amount. Rejecting.",
                    loanRequest));
            return 1.;
        }

        Double risk = 0.;

        if (.4 * loanRequest.getClient().getSalary() * loanRequest.getTerm() < 1.36 * loanRequest.getAmount()) {
            risk += 0.47;
        }

        logger.debug(String.format("Estimated risk for request: %s is %f", loanRequest, risk));

        return risk;
    }

}
