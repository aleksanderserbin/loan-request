package ru.aserbin.loanrequest.api.repository;

import java.util.List;

public interface Repository<T, K> {

    T save(T obj);
    T update(T obj);
    T find(K key);
    List<T> findAll();
    void delete(T obj);

}
