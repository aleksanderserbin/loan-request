package ru.aserbin.loanrequest.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.aserbin.loanrequest.api.exception.ApiError;
import ru.aserbin.loanrequest.api.exception.NotFoundException;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {


    private final Logger logger
            = LoggerFactory.getLogger(ApiExceptionHandler.class);

    @Override
    protected ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        logger.debug("BAD REQUEST: ", ex);
        ApiError apiError = new ApiError("Request validation failed");
        apiError.setDetails(ex.getBindingResult().getAllErrors());
        return ResponseEntity.badRequest().body(apiError);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleServerException(HttpServletRequest req, Exception ex) {
        logger.error(String.format("Server error: %s", ex.getMessage()), ex);
        return ResponseEntity
                .status(500)
                .body(new ApiError("Internal Server Error"));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiError> handleNotFoundException(HttpServletRequest req, Exception ex) {
        logger.debug("NOT FOUND: ", ex);
        return ResponseEntity
                .status(404)
                .body(new ApiError("Not Found"));
    }




}
