package ru.aserbin.loanrequest.model;

import javax.persistence.*;

@Entity
@Table(name = "clients")
public class ClientData {

    private String documentId;
    private String name;
    private String lastName;
    private Integer salary;

    @Id
    @Column(name = "document_id", nullable = false)
    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }


    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "salary", nullable = false)
    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }
}
